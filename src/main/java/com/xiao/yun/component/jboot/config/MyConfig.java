package com.xiao.yun.component.jboot.config;

import com.jfinal.config.Constants;
import com.jfinal.config.Interceptors;
import com.jfinal.config.Routes;
import com.jfinal.render.ViewType;
import com.xiao.yun.component.beelt.XSSDefenseFormat;
import io.jboot.web.JbootAppConfig;
import org.beetl.core.GroupTemplate;
import org.beetl.ext.jfinal3.JFinal3BeetlRenderFactory;

/**
 * @Author 袁旭云【rain.yuan@transn.com】
 * Created by rain on 2018/5/3.
 * @Date 2018/5/3 14:52
 */
public class MyConfig extends JbootAppConfig {

    @Override
    public void configConstant(Constants me) {
        super.configConstant(me);
        me.setDevMode(true);
        me.setViewType(ViewType.JSP); // 设置视图类型为Jsp，否则默认为FreeMarker

        JFinal3BeetlRenderFactory rf = new JFinal3BeetlRenderFactory();
        rf.config();
        me.setRenderFactory(rf);

        // 获取GroupTemplate ,可以设置共享变量等操作
        GroupTemplate groupTemplate = rf.groupTemplate;
        groupTemplate.registerFormat("xss", new XSSDefenseFormat());

    }

    @Override
    public void configRoute(Routes me) {
        me.setBaseViewPath("/pages");
        super.configRoute(me);
    }

    @Override
    public void configInterceptor(Interceptors interceptors) {
        super.configInterceptor(interceptors);
    }

    @Override
    public void afterJFinalStart() {

        super.afterJFinalStart();

        /*JhhCache.init();

        // JFinal启动后 启动定时任务
        QuartzPlugin plugin = new QuartzPlugin();
        plugin.start();*/

        System.out.println("##################################");
        System.out.println("############系统启动完成##########");
        System.out.println("##################################");
    }

    @Override
    public void beforeJFinalStop() {
        super.beforeJFinalStop();

        // 关闭模板
        //BeetlRenderFactory.groupTemplate.close();

        new JFinal3BeetlRenderFactory().groupTemplate.close();

        System.out.println("##################################");
        System.out.println("############系统停止完成##########");
        System.out.println("##################################");
    }
}
