package com.xiao.yun.component.beelt;

import org.apache.commons.lang.StringEscapeUtils;
import org.beetl.core.Format;

/**
 * @Author 刘沛然
 * @Date 2017/9/25
 */
public class XSSDefenseFormat  implements Format {
    @Override
    public Object format(Object o, String s) {
        if (null == o) {
            return null;
        } else {
            return StringEscapeUtils.escapeHtml((String) o);
        }
    }
}
