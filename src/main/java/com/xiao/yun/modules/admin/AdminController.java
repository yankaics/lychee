package com.xiao.yun.modules.admin;

import io.jboot.web.controller.JbootController;
import io.jboot.web.controller.annotation.RequestMapping;
import sun.reflect.generics.tree.VoidDescriptor;

/**
 * 后台管理入口
 *
 * @Author 袁旭云【rain.yuan@transn.com】
 * Created by rain on 2018/5/10.
 * @Date 2018/5/10 15:25
 */
@RequestMapping("/admin")
public class AdminController extends JbootController {


    public void index() {
        /*if (getSessionUser() != null) {
            // 如果session存在，不再验证
            redirect(homePage);
        } else {
            render(loginPage);
        }*/
        render("/pages/admin/login.html");

    }

    public void login() {
        String username = this.getPara("username");
        String password = this.getPara("password");
        String code = this.getPara("code");
        if ("12345".equals(code)) {
            redirect("/admin/home");
        } else {
            render("/pages/admin/login.html");
        }
    }

    public void home() {
        render("/pages/index.html");
    }

    public void main() {
        render("/pages/admin/main.html");
    }

}
